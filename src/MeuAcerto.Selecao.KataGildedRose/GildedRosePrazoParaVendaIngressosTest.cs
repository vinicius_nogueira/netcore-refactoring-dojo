using System.Collections.Generic;
using Xunit;

namespace MeuAcerto.Selecao.KataGildedRose
{
    public class GildedRosePrazoParaVendaIngressosTest
    {
        [Fact]
        public void prazoParaVenda()
        {
            IList<Item> Items = new List<Item> { new Item { Nome = "Ingressos para o concerto do Turisas", PrazoParaVenda = 15, Qualidade = 6 } };
            GildedRose app = new GildedRose(Items);
            app.AtualizarQualidade();
            Assert.Equal(14, Items[0].PrazoParaVenda);
        }
    }
}
