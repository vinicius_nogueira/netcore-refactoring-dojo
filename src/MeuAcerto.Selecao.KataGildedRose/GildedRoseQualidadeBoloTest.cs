using System.Collections.Generic;
using Xunit;

namespace MeuAcerto.Selecao.KataGildedRose
{
    public class GildedRoseQualidadeBoloTest
    {
        [Fact]
        public void qualidadeBolo()
        {
            IList<Item> Items = new List<Item> { new Item { Nome = "Bolo de Mana Conjurado", PrazoParaVenda = 0, Qualidade = 6 } };
            GildedRose app = new GildedRose(Items);
            app.AtualizarQualidade();
            Assert.Equal(4, Items[0].Qualidade);
        }
    }
}
