using System.Collections.Generic;
using Xunit;

namespace MeuAcerto.Selecao.KataGildedRose
{
    public class GildedRoseQualidadeIngressosTest
    {
        [Fact]
        public void qualidade()
        {
            IList<Item> Items = new List<Item> { new Item { Nome = "Ingressos para o concerto do Turisas", PrazoParaVenda = 1, Qualidade = 6 } };
            GildedRose app = new GildedRose(Items);
            app.AtualizarQualidade();
            Assert.Equal(9, Items[0].Qualidade);
        }
    }
}
