using System.Collections.Generic;
using Xunit;

namespace MeuAcerto.Selecao.KataGildedRose
{
    public class GildedRoseQualidade
    {
        [Fact]
        public void qualidade()
        {
            IList<Item> Items = new List<Item> { new Item { Nome = "fixme", PrazoParaVenda = 0, Qualidade = 0 } };
            GildedRose app = new GildedRose(Items);
            app.AtualizarQualidade();
            Assert.Equal(0, Items[0].Qualidade);
        }
    }
}
