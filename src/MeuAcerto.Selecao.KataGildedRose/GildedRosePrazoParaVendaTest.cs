using System.Collections.Generic;
using Xunit;

namespace MeuAcerto.Selecao.KataGildedRose
{
    public class GildedRosePrazoParaVendaTest
    {
        [Fact]
        public void prazoParaVenda()
        {
            IList<Item> Items = new List<Item> { new Item { Nome = "fixme", PrazoParaVenda = 1, Qualidade = 0 } };
            GildedRose app = new GildedRose(Items);
            app.AtualizarQualidade();
            Assert.Equal(0, Items[0].PrazoParaVenda);
        }
    }
}
