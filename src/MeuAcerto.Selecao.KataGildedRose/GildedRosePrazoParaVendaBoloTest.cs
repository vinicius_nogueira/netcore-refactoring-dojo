using System.Collections.Generic;
using Xunit;

namespace MeuAcerto.Selecao.KataGildedRose
{
    public class GildedRosePrazoParaVendaBoloTest
    {
        [Fact]
        public void prazoParaVendaBolo()
        {
            IList<Item> Items = new List<Item> { new Item { Nome = "Bolo de Mana Conjurado", PrazoParaVenda = 0, Qualidade = 6 } };
            GildedRose app = new GildedRose(Items);
            app.AtualizarQualidade();
            Assert.Equal(-1, Items[0].PrazoParaVenda);
        }
    }
}
