﻿﻿using System.Collections.Generic;

namespace MeuAcerto.Selecao.KataGildedRose
{
    class GildedRose
    {
        IList<Item> Itens;
        public GildedRose(IList<Item> Itens)
        {
            this.Itens = Itens;
        }

        public void AtualizarQualidade()
        {
            for (var i = 0; i < Itens.Count; i++)
            {
                if (Itens[i].Nome != "Dente do Tarrasque")                
                {
                    
                    if (Itens[i].Nome == "Bolo de Mana Conjurado" && Itens[i].Qualidade > 0)
                    {
                        Itens[i].Qualidade = Itens[i].Qualidade - 2;
                    }
                    else
                    {
                        if (Itens[i].Nome != "Queijo Brie Envelhecido" && Itens[i].Nome != "Ingressos para o concerto do Turisas")
                        {
                            if (Itens[i].Qualidade > 0)
                            {
                                Itens[i].Qualidade = Itens[i].Qualidade - 1;
                            } 
                             if (Itens[i].PrazoParaVenda <= 0 && Itens[i].Qualidade > 0)
                            {
                                Itens[i].Qualidade = Itens[i].Qualidade - 1;
                            }
                        }
                        else
                        {
                            if (Itens[i].Qualidade < 50)
                            {
                                if (Itens[i].Nome == "Ingressos para o concerto do Turisas" || Itens[i].Nome == "Queijo Brie Envelhecido")
                                {
                                    Itens[i].Qualidade = Itens[i].Qualidade + 1;
                                    if (Itens[i].Nome == "Ingressos para o concerto do Turisas")
                                    {
                                        if (Itens[i].PrazoParaVenda < 11)
                                        {
                                            if (Itens[i].Qualidade < 50)
                                            {
                                                Itens[i].Qualidade = Itens[i].Qualidade + 1;
                                            }
                                        }

                                        if (Itens[i].PrazoParaVenda < 6)
                                        {
                                            if (Itens[i].Qualidade < 50)
                                            {
                                                Itens[i].Qualidade = Itens[i].Qualidade + 1;
                                            }
                                        }
                                    }

                                    if(Itens[i].Nome == "Queijo Brie Envelhecido" && Itens[i].Qualidade < 50 && Itens[i].PrazoParaVenda <= 0)
                                    {
                                        Itens[i].Qualidade = Itens[i].Qualidade + 1;
                                    }
                                }
                            }
                        }

                        if (Itens[i].PrazoParaVenda <= 0 && Itens[i].Nome == "Ingressos para o concerto do Turisas")
                        {
                            Itens[i].Qualidade = Itens[i].Qualidade - Itens[i].Qualidade;
                        }
                    }
                    Itens[i].PrazoParaVenda = Itens[i].PrazoParaVenda - 1;
                }
            }
        }
    }
}
