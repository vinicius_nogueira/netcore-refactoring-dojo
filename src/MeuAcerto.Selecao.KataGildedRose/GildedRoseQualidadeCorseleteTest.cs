using System.Collections.Generic;
using Xunit;

namespace MeuAcerto.Selecao.KataGildedRose
{
    public class GildedRoseQualidadeCorseleteTest
    {
        [Fact]
        public void qualidade()
        {
            IList<Item> Items = new List<Item> { new Item { Nome = "Corselete +5 DEX", PrazoParaVenda = -1, Qualidade = 6 } };
            GildedRose app = new GildedRose(Items);
            app.AtualizarQualidade();
            Assert.Equal(4, Items[0].Qualidade);
        }
    }
}
