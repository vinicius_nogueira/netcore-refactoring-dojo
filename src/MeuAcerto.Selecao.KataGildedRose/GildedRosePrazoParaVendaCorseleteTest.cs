using System.Collections.Generic;
using Xunit;

namespace MeuAcerto.Selecao.KataGildedRose
{
    public class GildedRosePrazoParaVendaCorseleteTest
    {
        [Fact]
        public void prazoParaVenda()
        {
            IList<Item> Items = new List<Item> { new Item { Nome = "Corselete +5 DEX", PrazoParaVenda = 0, Qualidade = 0 } };
            GildedRose app = new GildedRose(Items);
            app.AtualizarQualidade();
            Assert.Equal(-1, Items[0].PrazoParaVenda);
        }
    }
}
